###########
 Our Tasks
###########

This is an example on how to use lesana to manage a personal / few users
ticket tracker.

The file ``aliases.sh`` can be sourced (``. ./aliases.sh``) to provide
helpers for common searches.
