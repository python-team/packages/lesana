=========
lesana-rm
=========

SYNOPSIS
========

lesana rm [-h] [--collection COLLECTION] entries [entries ...]

DESCRIPTION
===========

Lesana rm removes an entry from the collection, removing both the file
and the cached entry.

OPTIONS
=======

-h, --help
   Prints an help message and exits.
--collection COLLECTION, -c COLLECTION
   The collection to work on. Default is ``.``

